# README

Dear OSP designers,

we are sending here our introductory text of our project ("MELT_ritualsagainstbarriers.txt") as well as a poster ("MELT_RitualsAgainstBarriers.pdf").
You will also find two photos with a sketch for how we would like the text and poster to be layed out in the newspaper. Our idea would be to use the centerfold 
of the newspaper for the poster, and to place the introductory text on the page before (so basically the poster is on the backside of the text). This way, people could "pull out" the poster from the newspaper.
We will need the final dimension for the poster. 

With our kindest wishes
MELT