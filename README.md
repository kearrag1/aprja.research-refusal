# aprja.research-refusal

*italic* text in between single ampersands will be italic
**bold** text in between two asterisks will be bold

Lists

- list item
- second list item
- third list item

Or, numbered

1. List item
2. List item
3. List item


Blockquotes

> Text preceded by a greater than sign is treated
> as a blockquote, just like in email

Header:
# Biggest Header
## Header
### Smaller Header
...
###### Smallest header

Links
<http://example.com/> will become a link
[label to be displayed](http://example.com/) will become a link with 'label to be displayed' as text

Images
![alternative text](path/to/file.jpg)

