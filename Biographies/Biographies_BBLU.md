## Bad Bitch Link Up: Connecting Black Feminist Genealogies

### Biographies

***alea adigweme***

alea adigweme is a multidisciplinary Igbo-Vincentian cultural worker active in the fields of creative writing, book arts, performance, installation, and visual media. Her creative and critical labors are undergirded by interests in archives, the politics of pleasure, the (hyper/in)visibility of Blackness, and the lived experiences and cultural work of femmes of perceivable African descent, all of which have influenced her extensive public-facing work as an interviewer, moderator, panelist, and educator. She is currently a second-year MFA student in interdisciplinary studio art at UCLA, where she's working on a seris of projects about Black femme labor. She currently lives in Tovaangar, the unceded Tongva land commonly called Los Angeles. 

***Catherine Feliz***

Catherine Feliz is an interdisciplinary artist born and raised in Lenape territory (NYC) to parents from Kiskeya Ayiti (Dominican Republic). Intersectional feminist theory, archival research, and earth-based healing inform their practice. They work to reclaim ancestral technologies that have been systematically erased by drawing from multiple disciplines to unearth histories and make space for decolonial futures. Employing techniques of framing, opacity, desire, and language, they work in a variety of mediums including installation, bookmaking, video, text, and fabric. Visit [Botánica Cimarrón](http://botanicacimarron.love/) to find their plant spirit medicines, classes and more. They are also the co-founder of [Abuela Taught Me](https://www.instagram.com/abuelataughtme/), an Afro-Taino Two-Spirit educational space, and a founding member of [Homecoming](https://homecomingcollective.com/), a QTBIPOC radical care collective. Catherine is currently an MFA candidate at the University of California, Los Angeles in the department of Interdisciplinary Studio. 

***Kearra Amaya Gopee***

[Kearra Amaya Gopee](https://kearramaya.com/) is an anti-disciplinary visual artist from Carapichaima, Kairi (the larger of the twin island nation known as Trinidad and Tobago), living and working on unceded Tongva land (Los Angeles, CA). Their research based practice focuses on violence as it exists in/is enacted on the Anglophone Caribbean and its diasporas. They render this violence elastic and atemporal--leaving ample room for the consideration and manipulation of its history, immediacy and possible generative afterlives. Using lived experiences as a point of departure, they address violence’s impact on themes of (post)coloniality, affect, migration, intergenerational trauma, queerness, difference and healing.  Through their interventions, they aim to temper what we have known to be true with the potential of intuitive knowledges that have been historically cast aside in favour of Western assimilation. Currently, they are a MFA candidate at University of California, Los Angeles.

***A.E. Stevenson***

A.E. Stevenson is a Ph.D. candidate in Cinema and Media Studies at the University of California, Los Angeles. She is an anti-disciplinarian whose research pulls from art history, Black feminst studies, film studies, media studies, and phenomenology. She is currently writing her dissertation, “Niggas on the Internet: Scenes of a Black Social Life,” where, through an analysis of Vine, TikTok, the Shade Room, and the music videos of Solange, she argues that Black people have fundamentally changed the visual language of the internet. Her most recent article, “A Sleight of Hair: Chaotic Strands of Emodiment in Sanaa Hamri’s Something New,” can be found in Feminist Media Histories 6.4. 
