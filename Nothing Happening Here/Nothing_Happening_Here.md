# Nothing but Debts: Performing the Neo-Institution

Our original proposal to the Research Refusal CFP was titled “Nothing Happening Here: From Research Refusals to Studying Together.” Yet even after we submitted, the provenance of our title was not entirely clear to us. “Nothing” made sense, since we'd worked on nothing together before. In “Happening,” there seemed to be a gesture towards 1960s “Happenings” and perhaps a future promise of performativity. But what about “Here”? Where did that come from? Where was "here," and what did it signify?

We began the conclusion to our first listserv email with: “This contradiction between togetherness and individuality is something we will explore further in subsequent provocations, but for now, let us make our own position clear...” (11/30/2020). Yet despite striving for clarity, in the preceding sentences, we qualified these same we’s, us’s, and our’s. In short, we attempted to recognize the disparity of our varying positions by writing: 
We are all in this together. Or we should say, we are each in this together. Because we also want to acknowledge our differentiated positions: first within our group, and then across all nine groups, and finally amongst the organizers. We want to problematize the too-easy use of the word “we.” We ask you to be suspicious of those who say “we”—including us!
Even our own internal “we” remained protean. Our five positions shifted, open and fluid, over discussions in email, Whatsapp, Zoom. We continued to turn over and over where we each stood: with each other, with the other groups, and with/against institutions. We even struggled to agree on a collective definition of the word “institution” and whether this word could be applied to transmediale.

In the new year, we tried yet again. We took up the question of heres in letters, a chain of messages spanning several days and many more timezones. Our unfixable heres continued to ask for some address, though our multiplicity was irresolvable. In our writing, the contours of our multiple heres became more clear, but this raised further questions. What about the heres shared amongst the nine groups? And what about the entirely unknowable here of transmediale itself? Unknowable since none of us would travel to transmediale this year, meaning the here we were hoping to both partake in and refuse was an absence, a nothing. Here never settled anywhere.

## ™ as a Neo/Non-Institution
Transmediale (hereafter abbreviated ™) brings to light a form of institution that is distinct from the public institution Athena Athanasiou sees as imperiled by its neoliberal privatization—even while being publicly funded. To take on Athanasiou’s call to perform the institution “as if it were possible” (p. 682), how to “resist”, “reinvent”, “reform”, “re-institute” something that does not offer any grip?  

In the past five months, the nine groups selected by ™ produced a  significant amount of outputs in the form of texts exchanged on the ™ listserv, presentations, and a brilliant zine. The generosity in sharing, the depth of thoughts, the individual engagement, the richness of the exchanges—all of it is something I had never experienced before. Yet, the names of the participants—those proper names by the citation of which an institution can, at least symbolically, recognize the unpaid labor of its subjects—are nowhere to be found on the ™ website. After a request for clarification, an organizer (here O) responded “Full credits will appear at that time [the time of the publication of the newspaper], including on the transmediale website. We are simply waiting until the outputs are made public to do this.” What is implied here is that the research done, the work accomplished, and the knowledge shared up to now do not deserve public recognition, until the institution decides otherwise. In other words: not before yet more work has been done. However, the work has to be conceived of as “an opportunity, not an obligation.” 

™ is the most slippery object imaginable. Try to catch a wet fish! ™ doesn’t provide a structure to support the performing of (research) refusal. If refusal means—following Tuck and Yang—to “turn the gaze back upon power” (p. 241), how to refuse something that invisibilizes its power either by its being oblivious to it or by its refusal to take the responsibility that comes with it? 

Careful however: this is not an attack on individual O, themselves taken in the slippery nets of the structure. Instead, this text is an attempt to make the flimmering, glittery structure of the neo/non-institution visible. Refusal is our ethical and political responsibility and this attempt is the closest we will come to refusing ™, for now. 

We are irritated with each other that we have complied in writing this, but we agree that to not comply “won’t mean much.” This irritation turns into responsibilization of each other, as we assign ourselves tasks of editing, writing, designing—work we all love to do—but not for nothing! We, the group Nothing Happening Here, have arrived at the neo-institution of ™ to find nothing already here—except for the responsibilization to become something.

We comply because ™ provides the conditions for our assembly, and we cannot do what we want to do, say what we want to say, assemble how we want to assemble without reinforcing the conditions we assemble against. As Judith Butler writes in Notes toward a Performative Theory of Assembly, “None of us acts without the conditions to act, even though sometimes we must act to install and preserve those very conditions. The paradox is obvious, and yet what we can see when the precarious assemble is a form of action that demands the conditions for action and living” (p. 16). 

Here, this newspaper, is both a condition for our precarity and an enactment against the neo-institution. In this very moment that you are reading we assemble to say: The neo/non-institution does not break. Made from a silicon-like material, very smooth to the touch, like a cake mold, it can be baked at high temperature and won't melt. You can deform it. But it will take back its shape as soon as you release the pressure.


## Performing Our Debts Together

The nine Research Refusal groups gathered in Zoom a second time in late January. This was the closest we would come to sharing a here—yet one more reminder of how over the past year, such disaggregated gatherings have become the numbing norm for realizing being-together. When we presented our research, we focused on acknowledging our debt to the other eight groups, recognizing their contributions to our thinking. We thought of this as a performance of both debt and collectivity. At the close of our talk, we addressed the assembled groups and took up once more the unsteadiness of our assembly, the uncertainty of our varied positions:
>We hardly know each other, after all. And so, how can our interests become common? Especially given our brief time together and the globe-spanning array of different heres that have been gathered here today.

Of our twenty allotted minutes, we held the last eight for a survey. Twenty-one people, including us, participated and everyone present in the Zoom session shared eight silent minutes that otherwise would have been Q&A. Even the chat became a space for critiquing and defending the survey as a means of assembly and a form for thinking with each other. 

We had already accepted that we could not yet claim common experience with our fellow refusers. Yet against the backdrop of the impossibility of building common interests within our various heres, we held out for small moments of community and sharing. As Athanasiou suggests in “Performing the Institution ‘As If It Were Possible’”:
>The conditions of possibility for being-in-common are being destroyed by the institutional forces of dispossession that underlie the contemporary regime of neoliberal rationality. And yet, induced precarity can serve as an ethico-political resource for effecting responsive modes of being-in-common, whereby a certain impossibility of being-in-common might also be shared. (p. 680)

These institutional forces of dispossession come in many forms: racism, sexism, neoliberalism, heteronormativity, and patriarchy being some of the most visible and destructive. But dispossession also makes itself felt in more insidious ways, especially among purported equals. Sharing a here with someone is hard when there are unspoken hierarchies, unchallenged norms, and unreflected positionalities—in other words, when you don’t actually share a here at all. But among those who are dispossessed, whether in ways large or small, Athanasiou promises a potential “being-in-common,” even as she recognizes its very impossibility.


Donna Haraway expressed a similar sentiment in Situated Knowledges: The Science Question in Feminism and the Privilege of Partial Perspective. Amidst a debate between deconstructive relativism and what Haraway called “a no-nonsense commitment to faithful accounts of a ‘real’ world” (p. 579), she proposes a balancing act similar to Athanasiou's stance on the institution. Haraway describes the self as “partial in all its guises… always stitched together imperfectly” (p. 586). Yet because of, rather than despite, these sutures, Haraway holds on to the possibility of being able “to see together” while leaving open space for difference. As she says, “location is about vulnerability; location resists the politics of closure, finality” (p. 590). Thus, our uncertainty around heres, combined with our commitment to holding these different, sometimes vulnerable heres together, is an effort toward being-in-common. As Haraway says, “Situated knowledges are about communities, not about isolated individuals. The only way to find a larger vision is to be somewhere in particular” (p. 590). In the past year, we shared the experience of being scattered across innumerable particular places—all while striving to maintain our communities and, perhaps, build new ones.

In this spirit, we return to the survey on debt we did together, as nine groups, on January 21. Although we weren’t all there that day and although not everyone who was there took the survey, we still understand this (partial) collective exercise as an imperfect moment of when we were joined in expressing our mutual, yet individual, debts. The five questions of the survey and the twenty-one responses are scattered, fragmented, partially revealed as the backdrop to this text, their shimmering presence reflecting our struggle to find a here and the fragility of our shared hopes for instituting differently.

Nothing Happening Here, March 2021

Kelsey Brod  
Katia Schwerzmann  
Jordan Sjol  
Alexander Strecker  
Kristen Tapson  

https://nothinghappeninghere.work/

//Insert https://gitlab.com/osp-kitchen/aprja.research-refusal/-/blob/master/Nothing%20Happening%20Here/NHH_NWG.jpeg

_Photo caption: Seven researchers, from two different Refusal groups, not working and doing nothing together over Zoom._ 


### Indebted To
Athanasiou, Athena. “Performing the Institution ‘as if it Were Possible.’” *Former West: Art and the Contemporary After 1989*. Eds. Maria Hlavajova, and Simon Sheikh. Utrecht/Cambridge, MA: BAK/The MIT Press, 2016. 679-92.

Butler, Judith. *Notes Toward a Performative Theory of Assembly.* Cambridge, MA: Harvard University Press, 2015.

Haraway, Donna J. “Situated Knowledges: The Science Question in Feminism and the Privilege of Partial Perspective.” Feminist Studies 14.3 (1988): 575-99.

Harney, Stefano and Fred Moten. *The Undercommons: Fugitive Planning and Black Study.* Wivenhoe: Minor Compositions, 2013.

Harney, Stefano and Fred Moten. “Debt and Study.” e-flux 14 (2010): 1–5.

Tuck, Eve, and K. Wayne Yang. “R-Words: Refusing Research.” Humanizing Research: Decolonizing Qualitative Inquiry With Youth and Communities. Eds. Django Paris, and Maisha T. Winn. Thousand Oaks, CA: SAGE, 2014. 223-47.



