# How Can I Live? Only in Refusal. 

### _References_

 1. _[The Anarchy of Colored Girls Assembled in a Riotous Manner](https://mumbletheoryhome.files.wordpress.com/2019/10/the-anarchy-of-colored-girls.pdf)_ by Saidiya Hartman. The South Atlantic Quarterly, July 2018

 2. _[Chocolate Babies](https://vimeo.com/62632619)_ by Stephen Winter (1996) **tw: drug use, anti-asian racism, blood violence**

I’m pushing these words out in conflict with my mind that refuses to write, refuses to squeeze. Or is it something different? I’m feeling resistant to authentically engaging with this digest. A deep exhaustion of expressive sensical labor that persists like an army under water even as the threads of human life and the so called US political life are pulled apart. Re-fractured life goes on within a pandemic that rages across Los Angeles and as white supremacist terrorists plan armed rebellions across the colonial capitals. So in my refusal I pleasurably lounge around my kitchen and I also return to one of my favorite movies, Chocolate Babies. I recommend watching after reading and in reflection of Saidiya Hartman’s piece The Anarchy of Colored Girls Assembled in a Riotous Manner. For Black life has always refused because we’ve had no other choice if we dare to live. 

> **In the direct action of the collective of friends in the film, “black faggots with a political agenda”, we can feel reflected back to us the creative imagination practicing freedom in “a common tongue”.**

The refusal to be worked is the art of living for Black, Queer, Femme, Poor, Differently Abled, Undocumented, Overly-documented lives that survive in mutual aid and shadowed kinships. When Lady Marmalade says to a hidden crowd at a club “I got AIDS dammit and I got it by sucking dick and fucking sticking needles in my arms and the government doesn’t give a shit”, she refuses her marked disposability that have funneled countless others into social death. I won’t spoil the film for anyone so I’ll stop there, but leave you with these set of questions posed by Hartman reflecting on riots at the Bedford Hills Correctional Facility in 1917 and several years to follow: “What to make of the utopian impulse that enabled them to believe that anyone cared about what they had to say? What convinced them that the force of their collective utterance was capable of turning anything around? What urged them to create a reservoir of living within the prison’s mandated death? What made them tireless?”

Warmly,

Catherine Feliz

alea adigweme

A.E Stevenson

Kearra Amaya Gopee

BBLU
