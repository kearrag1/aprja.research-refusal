Hello Gijs and Ludivine!

Rosie and I made a 2 page zine and uploaded a pdf at screen resolution. We used 279 × 432 mm dimensions but can adjust to match the dimensions you will go for. It would be great if you could let us know what the final dimensions will be, so we can provide you with print resolution pdfs (or another format if that is more convenient for you).

We added our annotated bibliography and group info as markdown. 

The design was done by Dan Green, in case there is a credits page.

Let us know if anything is missing or needs to be adjusted. Looking forward to seeing the publication come together :)

All the best,

Marloes & Rosie
