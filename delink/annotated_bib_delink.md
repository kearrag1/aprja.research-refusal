Reiter, Bernd, (Ed.). (2018). Constructing the Pluriverse. Durham: Duke University Press. 

Walter Mignolo has contribued two texts to this publication, ‘Foreword. On Pluriversality and Multipolarity’ and ‘On Pluriversality and Multipolar World Order’ which are grounded in his elaboration of the pluriverse and identify delinking as a strategy towards the pluriverse. We have taken delinking as refusal as the basis of our enquiry, starting from delinking as a form of refusal of the globalised world, specifically in relation to technological infrastructures and technological/cultural practices.
