Berardi, Franco. *The Soul at Work*. MIT Press, 2009.
 
Deleuze, Gilles and Félix Guattari. *Anti-Edipus*. University of Minnesota Press, 1983..
 
Ernst, Wolfgang. *Digital Memory and The Archive*. University of Minnesota Press, 2013.
 
Fisher, Mark. *Capitalist Realism*. Zero Books, 2009.
 
Galloway, Alexander, and Eugene Thacker. *The Exploit*. University of Minnesota Press, 2007.
 
Heisenberg, Werner. *Physics and Philosophy*. Harper & Brothers, 1958.
 
Lazzarato, Maurizio. *Signs and Machines*. MIT Press, 2014.
 
Marx, Karl. *Grundrisse*. Penguin Books, 1973.
 
Simondon, Gilbert. *Individuation in the Light of Notions of Form and Information*. University of Minnesota Press, 2020.
 
Sloterdijk, Peter. *Critique of Cynical Reason*. University of Minnesota Press, 1987.

Žižek, Slavoj. *The Sublime Object of Ideology*. Verso, 1989.
